type op = Add | Sub | Mult | Div | Equal | Neq | Less | Leq | Greater | Geq

type elem_selector =
    ElemSelector of string * string * string * string

type attr_selector =
    AttrSelector of string
  | NoAttr

type expr =
    IntLiteral of int
  | StringLiteral of string
  | DoubleLiteral of float
  | ArrayLiteral of expr list
  | BoolLiteral of bool
  | Id of string
  | Binop of expr * op * expr
  | Assign of string * expr
  | ArrayAssign of string * int * expr
  | AssignOp of string * op * expr
  | DoubleOp of string * op * expr
  | Call of string * expr list
  | Selector of elem_selector list * attr_selector
  | UrlConstructor of expr
  | ArrayAccess of string * int
  | Noexpr
  
type vdecl = 
    Int of string * expr (* name, initial value, has initial value  *)
  | Bool of string * expr
  | Str of string * expr 
  | Double of string * expr
  | Url of string * expr
  | Sel of string * expr
  | Array of vdecl * int (* name, size -- Does not allow for array 
                              of arrays (I think this is too much to try for this 
                              project. Shall we cut it? - Matt) *)

type stmt =
    Block of stmt list
  | Vdecl of vdecl
  | Expr of expr
  | Return of expr
  | If of expr * stmt * stmt
  | For of string * expr * expr * expr * expr * stmt
  | While of expr * stmt
  | Loop of string * string * stmt
  | Nostmt

type func_decl = {
    fname : string;
    ftype : string;
    formals : vdecl list;
    body : stmt list;
  }

type program = vdecl list * func_decl list

(* Printing of AST -- for initial debugging only.
    Was superceded by SAST and Printer (after we figured out
    what they were). *)

let rec string_of_elem_selector = function
    ElemSelector(elem, classname, attr, attrval) -> "(new Selector(\"" ^ elem ^ "\",\"" ^ classname ^ "\",\"" ^ attr ^ "\",\"" ^ attrval ^ "\"))"

let rec string_of_attr_selector = function
    AttrSelector(s) -> "selector(" ^ s ^ ")"
  | NoAttr -> ""

let rec string_of_expr = function
    IntLiteral(l) -> string_of_int l
  | DoubleLiteral(d) -> string_of_float d
  | StringLiteral(s) -> "\"" ^ s ^ "\""
  | ArrayLiteral(l) -> "[" ^ String.concat ", " (List.map string_of_expr l) ^ "]"
  | BoolLiteral(l) -> (string_of_bool l) 
  | Id(s) -> s
  | ArrayAccess(id, index) -> id ^ "[" ^ string_of_int index ^ "]"
  | Binop(e1, o, e2) ->
      string_of_expr e1 ^ " " ^
      (match o with
	          Add -> "+" | Sub -> "-" | Mult -> "*" | Div -> "/"
          | Equal -> "==" | Neq -> "!="
          | Less -> "<" | Leq -> "<=" | Greater -> ">" | Geq -> ">=")
      ^ " " ^ string_of_expr e2
  | AssignOp(id, sign, e) -> id ^ "=" ^ id ^ (match sign with Add -> "+" | Sub -> "-" | Mult -> "*" | Div -> "/" | Equal -> "==" | Neq -> "!=" | Less -> "<" | Leq -> "<=" | Greater -> ">" | Geq -> ">=") ^ (string_of_expr e) 
  | DoubleOp(id, sign, e) -> id ^ (match sign with Add -> "++" | Sub -> "--" | _ -> "")
  | Assign(v, e) -> v ^ " = " ^ string_of_expr e
  | ArrayAssign(id, index, e) -> id ^ "[]=" ^ string_of_expr e
  | Call(f, el) ->
    (match f with
      | "print" ->
        "System.out.print(" ^ String.concat ", " (List.map string_of_expr el) ^ ")"
      | "println" ->
        "System.out.println(" ^ String.concat ", " (List.map string_of_expr el) ^ ")"
      | _ -> 
        "app." ^ f ^ "(" ^ String.concat ", " (List.map string_of_expr el) ^ ")")
  | Selector(e, a) -> List.fold_left (fun acc a -> string_of_elem_selector a ^ acc) "" e
        ^ string_of_attr_selector a
  | UrlConstructor(e) -> "new Url(" ^ string_of_expr e ^ ")"
  | Noexpr -> ""
 
(* Todo: These helper methods could be made more efficient *)
let rec string_of_vdecl_type = function
    Int(_,_) -> "int"
  | Double(_,_) -> "double"
  | Str(_,_) -> "String"
  | Url(_,_) -> "Url"
  | Sel(_,_) -> "Selector"
  | Array(ty,_) -> (string_of_vdecl_type ty) ^ "[]"
  | Bool(_,_) -> "Bool"

let rec string_of_vdecl_name = function
    Int(n,_) -> n
  | Double(n,_) -> n
  | Str(n,_) -> n
  | Url(n,_) -> n
  | Sel(n,_) -> n
  | Array(n,_) -> (string_of_vdecl_name n)
  | Bool(n,_) -> n
  
let rec string_of_vdecl_expr = function
    Int(_,e) -> string_of_expr e
  | Double(_,e) -> string_of_expr e
  | Str(_,e) -> string_of_expr e
  | Url(_,e) -> string_of_expr e
  | Sel(_,e) -> string_of_expr e
  | Array(e,s) -> (if s = 0 then "" else (string_of_vdecl_expr e))
  | Bool(_, e) -> string_of_expr e	
  
let rec string_of_vdecl = function
    Int(id, value) -> "int " ^ id ^ (if value != Noexpr then "=" ^ (string_of_expr value) else "") ^ ";"
  | Double(id, value) -> "double "^ id ^ (if value != Noexpr then "=" ^ (string_of_expr value) else "") ^ ";"
  | Str(id, value) -> "String " ^ id ^ (if value != Noexpr then "=" ^ (string_of_expr value) else "") ^ ";"
  | Url(id, value) -> "Url " ^  id ^ (if value != Noexpr then "=" ^ (string_of_expr value) else "") ^ ";"
  |	Sel(id, value) -> "Selector " ^  id ^ (if value != Noexpr then "=" ^ (string_of_expr value) else "") ^ ";"
  | Array(decl, size) ->  
        string_of_vdecl_type decl ^
        (match size with
            0 -> "[]"
          | size -> "[" ^ string_of_int size ^ "]") ^
        " " ^ string_of_vdecl_name decl ^
        (let expr_text = string_of_vdecl_expr decl in
          if String.length expr_text > 0 then "=" ^ expr_text else "") 
        ^ ";"
  | Bool(id, value) -> "boolean " ^ id ^ (if value != Noexpr then "="^ (string_of_expr value) else "") ^ ";"

let string_of_formals = function
    Int(id, value) -> "int " ^ id
  | Double(id, value) -> "double " ^ id
  | Str(id, value) -> "String " ^ id
  | Url(id, value) -> "url " ^ id
  | Sel(id, value) -> "Selector " ^ id
  | Array(decl, size) ->  
      string_of_vdecl_type decl ^
      (match size with
          0 -> "[]"
        | size -> "[" ^ string_of_int size ^ "]") ^
      " " ^ string_of_vdecl_name decl
  | Bool(id, value) -> "boolean " ^ id

let rec string_of_stmt = function
    Vdecl(vdecl) -> string_of_vdecl vdecl
  | Block(stmts) ->
      "{\n" ^ String.concat "\n" (List.map string_of_stmt stmts) ^ "}\n"
  | Expr(expr) -> string_of_expr expr ^ ";\n";
  | Return(expr) -> "return " ^ string_of_expr expr ^ ";\n";
  | If(e, s, Block([])) -> "if (" ^ string_of_expr e ^ ") " ^ string_of_stmt s
  | If(e, s1, s2) ->  "if (" ^ string_of_expr e ^ ")\n" ^
      string_of_stmt s1 ^ "else " ^ string_of_stmt s2
  | For(t, id, e1, e2, e3, s) ->
      "for (" ^ (if t="None" then "" else t) ^" " ^ string_of_expr e1  ^ " ; " ^ string_of_expr e2 ^ " ; " ^
      string_of_expr e3  ^ ") " ^ string_of_stmt s
  | Loop(arr, index, stmt) -> ""
  | While(e, s) -> "while (" ^ string_of_expr e ^ ") " ^ string_of_stmt s
  | Nostmt -> ""

let string_of_fdecl fdecl =
    "public static " ^ fdecl.ftype ^ " " ^ fdecl.fname ^ "(" ^ (if fdecl.fname = "main" then "String[] args" else "") ^ String.concat ", " (List.map string_of_formals fdecl.formals) ^ ") {\n" ^
    String.concat "\n" (List.map string_of_stmt fdecl.body) ^
    "}\n"
	
let string_of_program (vars, funcs) =
    "public class app {\n\n" ^
    String.concat "\n" (List.map string_of_vdecl (List.rev vars)) ^ "\n" ^
    String.concat "\n" (List.map string_of_fdecl (List.rev funcs)) ^
    "\n}"