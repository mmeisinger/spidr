{ 
	open Parser 

	(* Incicates whether the line is a blank line or not. If it is,
		it will not be parsed as a newline token. *)
	let lineHasKeyword = ref false
	
	(* need to save the state of whether we are parsing within a
	   selector to know whether to parse spaces as tokens or not *)
	let inSelector = ref false
}

rule token = parse
  ['\t' '\r'] { token lexbuf }
| (' ')* { if !inSelector = true then SPACE else token lexbuf }
| "/*"     { comment lexbuf }           (* Comments *)
| '\n'     { if !lineHasKeyword != true then 
					(lineHasKeyword := false; token lexbuf) 
				else 
					(lineHasKeyword := false; NEWLINE) }
| '('      { LPAREN }
| ')'      { RPAREN }
| '{'      { lineHasKeyword := false; LBRACE }
| '}'      { RBRACE }
| '['      { LBRACKET }
| ']'      { RBRACKET }
| ','      { COMMA }
| '.'      { PERIOD }
| '+'      { PLUS }
| "++"		{ PLUSPLUS }
| "+="		{ PLUSEQ }
| '-'      { MINUS }
| "--"		{ MINUSMINUS }
| "-="		{ MINUSEQ }
| '_'      { UNDERSCORE }
| '*'      { TIMES }
| "*="		{ TIMESEQ }
| '/'      { DIVIDE }
| "/="		{ DIVIDEEQ }
| '='      { ASSIGN }
| ';'      { SEMICOLON }
| ':'      { COLON }
| ('"')[^ '"']*('"') as string_decl  { lineHasKeyword := true; STRING_DEC(String.sub string_decl 1 ((String.length string_decl) - 2)) }
| ">"	   { GT }
| ">>"     { inSelector := false; RCARAT }
| "<"	   { LT }
| "<<"	   { inSelector := true; LCARAT }
| "=="     { EQ }
| "!="     { NEQ }
| "<="     { LEQ }
| ">="     { GEQ }
| "[]"     { ARRAYDEC }
| ['['] ['0'-'9']* [']'] as array_size { ARRAY_SIZE(int_of_string (String.sub array_size 1 ((String.length array_size) - 2))) }
| "if"     { IF }
| "else"   { ELSE }
| "for"    { FOR }
| "while"  { WHILE }
| "return" { RETURN }
| "string" { STRING }
| "int"    { INT }
| "url"    { URL }
| "double" { DOUBLE }
| "element" { ELEMENT }
| "selector" { SELECTOR }
| "function" { FUNCTION }
| "bool"	{ BOOLEAN }
| "true"	 { TRUE }
| "false" 	 { FALSE }
| "loop" 	{ LOOP }
| "@" { AT }
| ['0'-'9']+ as lxm { lineHasKeyword := true; LITERAL(int_of_string lxm) }
| ['0'-'9']* ['.'] ['0'-'9']* as lxm { lineHasKeyword := true; D_LITERAL(float_of_string lxm) }
| ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '0'-'9' '_']* as lxm { lineHasKeyword := true; ID(lxm) }
| eof { EOF }
| _ as char { lineHasKeyword := true; raise (Failure("illegal character " ^ Char.escaped char)) }

and comment = parse
  "*/"	{ token lexbuf }
| _		{ comment lexbuf }