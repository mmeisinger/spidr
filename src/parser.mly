%{ open Ast %}

%token NEWLINE NEWLINE_OPT
%token LPAREN RPAREN LBRACE RBRACE LBRACKET RBRACKET LCARAT RCARAT
%token COMMA SEMICOLON
%token AT PERIOD UNDERSCORE ARRAYDEC
%token <string> EL_ID
%token <string> EL_CLASS
%token <string> EL_ATTR
%token <string> EL_ATT
%token PLUS MINUS TIMES DIVIDE ASSIGN PLUSPLUS MINUSMINUS PLUSEQ MINUSEQ TIMESEQ DIVIDEEQ
%token EQ NEQ LT LEQ GT GEQ
%token RETURN IF ELSE FOR WHILE INT URL ELEMENT SELECTOR FUNCTION VOID DOUBLE STRING COLON LOOP
%token BOOLEAN TRUE FALSE
%token <string> STRING_DEC
%token <int> LITERAL
%token <float> D_LITERAL
%token <int> ARRAY_SIZE
%token <string> ID
%token EOF
%token SPACE

%nonassoc NEWLINE
%nonassoc NOELSE
%nonassoc NOVALUE
%nonassoc ELSE
%right ASSIGN
%left EQ NEQ
%left LT GT LEQ GEQ
%left PLUS MINUS PLUSPLUS
%left TIMES DIVIDE
%left ARRAYDEC COMMA
%left RCARAT
%right LCARAT
%right COLON

%start program
%type <Ast.program> program

%%

program:
   /* nothing */ { [], [] }
 | program vdecl { ($2 :: fst $1), snd $1 }
 | program fdecl { fst $1, ($2 :: snd $1) }

fdecl:
   FUNCTION ftype ID LPAREN formal_list RPAREN LBRACE stmt_list RBRACE
   { {    fname = $3;
          ftype = $2; 
          formals = List.rev $5;
          body = List.rev $8
   } }
  | 
   FUNCTION ftype ID LPAREN formal_list RPAREN NEWLINE LBRACE stmt_list RBRACE
   { {    fname = $3;
          ftype = $2; 
          formals = List.rev $5;
          body = List.rev $9
   } }

ftype:
    ID { $1 }
  | vtype { $1 }
  
vtype:
    INT               { "int" }
  | DOUBLE            { "double" }
  | STRING            { "string" }
  | URL               { "url" }
  | BOOLEAN           { "boolean" }
  | INT ARRAYDEC      { "int[]" }
  | DOUBLE ARRAYDEC   { "double[]" }
  | STRING ARRAYDEC   { "String[]" }
  | URL ARRAYDEC      { "url[]" }
  | BOOLEAN ARRAYDEC  { "boolean[]" }
  | SELECTOR          { "selector" }
  | SELECTOR ARRAYDEC { "selector[]" }

formal_list:
    /* nothing */ { [] } 
  | formal_list formals  { $2 :: $1 }
  | formal_list formals COMMA { $2 :: $1 }

array_dec:
    ARRAYDEC      { 0 }
  | ARRAY_SIZE    { $1 }
  
formals:
    INT ID                 { Int($2, Noexpr) }
  | INT array_dec ID       { Array(Int($3, Noexpr), $2) }
  | DOUBLE ID              { Double($2, Noexpr) } 
  | DOUBLE array_dec ID    { Array(Double($3, Noexpr), $2)  }
  | STRING ID              { Str($2, Noexpr) }
  | STRING array_dec ID    { Array(Str($3, Noexpr), $2) }
  | URL ID                 { Url($2, Noexpr) } 
  | URL array_dec ID       { Array(Url($3, Noexpr), $2) }
  | BOOLEAN ID             { Bool($2, Noexpr) }
  | BOOLEAN array_dec ID   { Array(Bool($3, Noexpr), $2) }
  | SELECTOR ID            { Sel($2, Noexpr) }
  | SELECTOR array_dec ID  { Array(Sel($3, Noexpr), $2) }

vdecl:
    INT ID initial_val NEWLINE { Int($2, $3) }
  | INT ARRAYDEC ID initial_val NEWLINE { Array(Int($3,$4), 0) }
  | DOUBLE ID initial_val NEWLINE { Double($2, $3) }
  | DOUBLE ARRAYDEC ID initial_val NEWLINE { Array(Double($3,$4), 0) }
  | STRING ID initial_val NEWLINE { Str($2, $3) }
  | STRING ARRAYDEC ID initial_val NEWLINE { Array(Str($3,$4), 0) }
  | URL ID initial_val NEWLINE { Url($2, $3) }
  | URL ARRAYDEC ID initial_val NEWLINE { Array(Url($3,$4), 0) }
  | BOOLEAN ID initial_val NEWLINE { Bool($2, $3) }
  | BOOLEAN ARRAYDEC ID initial_val NEWLINE { Array(Bool($3,$4), 0) }
  | SELECTOR ID initial_val NEWLINE { Sel($2, $3) }
  | SELECTOR ARRAYDEC ID initial_val NEWLINE { Array(Sel($3,$4), 0) }

initial_val:
  /* nothing */        { Noexpr }
  | ASSIGN expr        { $2 }
  | ASSIGN ARRAYDEC    { ArrayLiteral([]) }
  | ASSIGN ARRAY_SIZE  { ArrayLiteral([IntLiteral($2)]) }

stmt_list:
    /* nothing */  { [] }
  | stmt_list stmt { $2 :: $1 }
  
stmt:
    vdecl { Vdecl($1) }
  | expr NEWLINE { Expr($1) }
  | RETURN expr NEWLINE { Return($2) }
  | LBRACE stmt_list RBRACE { Block(List.rev $2) }
  | IF LPAREN expr RPAREN stmt %prec NOELSE { If($3, $5, Block([])) }
  | IF LPAREN expr RPAREN stmt ELSE stmt { If($3, $5, $7) }
  | FOR LPAREN vtype assign_for SEMICOLON boo_exp SEMICOLON for_incr RPAREN stmt  /*loop var declared in loop*/
     { For($3, (List.hd (List.tl $4)), (List.hd $4), $6, $8, $10) }
  | FOR LPAREN assign_for SEMICOLON boo_exp SEMICOLON for_incr RPAREN stmt   /*if loop var is declared outside of loop*/
     { For("None", (List.hd (List.tl $3)), (List.hd $3), $5, $7, $9) }
  | WHILE LPAREN boo_exp RPAREN stmt { While($3, $5) }
  | LOOP LPAREN ID ID RPAREN stmt { Loop($3, $4, $6) }
  | incr NEWLINE { Expr($1) }

boo_exp:
    expr EQ   expr { Binop($1, Equal, $3) }
  | expr NEQ  expr { Binop($1, Neq,   $3) }
  | expr LT   expr { Binop($1, Less,  $3) }
  | expr LEQ  expr { Binop($1, Leq,   $3) }
  | expr GT   expr { Binop($1, Greater,  $3) }
  | expr GEQ  expr { Binop($1, Geq,   $3) }
  
assign_for:
    ID ASSIGN expr   { [Assign($1, $3); Id($1)] }
  
assign:
    ID ASSIGN expr   { Assign($1, $3) }
  | ID ARRAY_SIZE ASSIGN expr { ArrayAssign($1, $2, $4) }
  
for_incr:
    incr       { $1 }
  | double_op  { $1 }

incr:    
    ID PLUSEQ expr    { AssignOp ( $1, Add , $3 ) }
  | ID MINUSEQ expr   { AssignOp( $1, Sub, $3 ) }  
  | ID TIMESEQ expr   { AssignOp( $1, Mult, $3 ) }  
  | ID DIVIDEEQ expr  { AssignOp( $1, Div, $3 ) } 

double_op:
    ID PLUSPLUS       { DoubleOp( $1 , Add , IntLiteral(1) ) }
  | ID MINUSMINUS     { DoubleOp( $1 , Sub, IntLiteral(1) ) }
  
array_literal:
  LBRACKET array_value RBRACKET { $2 }
  
array_value:
  /* nothing */ { [] }
  | expr COMMA array_value { $1 :: $3 }
  | expr %prec NOVALUE { [$1] } 

expr:
    LITERAL          { IntLiteral($1) }
  | D_LITERAL        { DoubleLiteral($1) }
  | array_literal    { ArrayLiteral ($1) }
  | STRING_DEC       { StringLiteral($1) }
  | ID ARRAY_SIZE    { ArrayAccess($1, $2)}
  | TRUE             { BoolLiteral(true) }
  | FALSE            { BoolLiteral(false)}
  | ID               { Id($1) }
  | boo_exp          { $1 }
  | assign           { $1 }
  | expr PLUS   expr { Binop($1, Add,   $3) }
  | expr MINUS  expr { Binop($1, Sub,   $3) }
  | expr TIMES  expr { Binop($1, Mult,  $3) }
  | expr DIVIDE expr { Binop($1, Div,   $3) }
  | double_op        { $1 }
  | COLON expr       { UrlConstructor($2) }
  | ID LPAREN actuals_opt RPAREN  { Call($1, $3) }
  | LPAREN expr RPAREN            { $2 }
  | LCARAT selector RCARAT        { $2 }
  
actuals_opt:
    /* nothing */ { [] }
  | actuals_list  { List.rev $1 }

actuals_list:
    expr                    { [$1] }
  | actuals_list COMMA expr { $3 :: $1 }

/*  Selector declarations
    Expected in format: element.class[attrname=attrvalue] */
selector:
    elem_selector_list AT ID { Selector($1, AttrSelector($3)) }
  | elem_selector_list       { Selector($1, NoAttr) }

elem_selector_list:
    elem_selector            { [$1] }
  | elem_selector_list SPACE elem_selector { $3 :: $1 }

elem_selector:
    elem_selector_name_class { let (a,b) = $1 in ElemSelector(a, b, "", "")}
  | elem_selector_att { let (c,d) = $1 in ElemSelector("", "", c, d)}
  | elem_selector_name_class elem_selector_att { let (a,b) = $1 in let (c,d) = $2 in ElemSelector(a, b, c, d)}

elem_selector_name_class:
    ID            { ($1, "") }
  | PERIOD ID     { ("", $2) }
  | ID PERIOD ID  { ($1, $3) }

elem_selector_att:
    LBRACKET ID RBRACKET { ($2, "") }
  | LBRACKET ID ASSIGN STRING_DEC RBRACKET { ($2, $4) }