open Ast
module StringMap = Map.Make(String)

exception Function_Name_Already_Exists of string
exception Variable_Name_Already_Exists of string
exception Undeclared_Identifier of string
exception Array_Of_Arrays_Not_Allowed
exception Mismatched_Types of string
exception Not_Implemented
exception Type_Expected of string list
exception Invalid_Operation of string
exception Function_Not_Found of string
exception Index_of_NonArray of string
exception Invalid_Arguments of string
exception Initalizer_List_On_NonArray of string
exception Array_Not_Initialized of string (* name of identifier *)
exception LoopRequiresArrayTarget of string
exception Invalid_Return_Type of string list
exception Expression_Expected of string
exception Invalid_Return_Type of string list
exception Expected_No_Return
exception Invalid_Init_List_Arg
exception Variable_Not_Initalized of string

let type_of_op op = match op with
    Ast.Add -> "Arithmetic"
  | Ast.Sub -> "Arithmetic"
  | Ast.Mult -> "Arithmetic"
  | Ast.Div -> "Arithmetic"
  | Ast.Equal -> "Relational"
  | Ast.Neq -> "Relational"
  | Ast.Less -> "Relational"
  | Ast.Leq -> "Relational"
  | Ast.Greater -> "Relational"
  | Ast.Geq -> "Relational"

type t =
  Void
| Bool
| Int
| Str
| Double
| Url
| Sel of bool (* bool whether has an attribute attached to it *)
| Arr of t * int option (* type of array and size of array (if size=None then array is uninstantiated) *)
| Elements (* Internal type only. Collection of elements when a URL is applied to a 'Sel' *)

type s_expr =
  S_IntLiteral of int
| S_StringLiteral of string
| S_DoubleLiteral of float
| S_ArrayLiteral of s_expr list * int
| S_BoolLiteral of bool
| S_Id of string * bool
| S_Binop of s_expr * op * s_expr
| S_Assign of string * s_expr
| S_ArrayAssign of string * int * s_expr
| S_AssignOp of string * op * s_expr
| S_DoubleOp of string * op * s_expr
| S_Call of string * s_expr list
| S_PrintArray of s_expr
| S_Selector of Ast.elem_selector list * Ast.attr_selector
| S_UrlConstructor of s_expr
| S_ArrayAccess of string * int
| S_ArrayConcat of s_expr * s_expr
| S_Noexpr
| S_ApplySelector of bool * s_expr * s_expr (* will return string array, url or elements, selector *)

type s_var_decl = {
  s_vname: string;
  s_vtype : t;
  mutable s_has_value : bool; (* tracks whether it has been initialized yet *)
  s_initial_value : s_expr;
  (* removed array size from here, since it whether a variable is an array is needed many more
     places in type checking, so it needs to be a part of the type *)
  (*s_array_size : int option;*) (* is None if not an array, else Some(arraysize) *)
  (* Initial values are instantiated...
      Globals: at beginning of main function
      In functions: where they appear in the code *)
}

type s_stmt =
  S_Block of s_stmt list
| S_Vdecl of s_var_decl
| S_Expr of s_expr
| S_Return of s_expr * t
| S_If of s_expr * s_stmt * s_stmt
| S_For of t * s_expr * s_expr * s_expr * s_stmt
| S_While of s_expr * s_stmt
| S_Loop of string * s_var_decl * s_stmt
| S_Nostmt

type s_func_decl = {
  s_fname : string;
  s_return_type : t;
  s_formals : s_var_decl list;
  mutable s_body: s_stmt list;
}

type s_symbol_table = {
  mutable parent: s_symbol_table option;  (* parent scope *)
  mutable variables: s_var_decl list;
}


type s_env_def = {
  mutable functions : s_func_decl StringMap.t;    (* function definitions *)
  mutable scope : s_symbol_table;                (* variables (also contains reference to parent scope) *)
  mutable has_selectors : bool;
  mutable has_array_concat : bool;
}

let include_built_in_functions map =
  StringMap.add "print" { s_fname = "print";
      s_return_type = Void;
      s_formals = 
            [{  s_vname = "message";
                s_vtype = Str;
                s_has_value = false;
                s_initial_value = S_Noexpr;
            }];
      s_body = []
  }
  (StringMap.add "println" { s_fname = "println";
  s_return_type = Void;
  s_formals = 
        [{  s_vname = "message";
            s_vtype = Str;
            s_has_value = false;
            s_initial_value = S_Noexpr;
        }];
  s_body = []
  }
  (StringMap.add "live" { s_fname = "live";
      s_return_type = Bool;
      s_formals = 
            [{  s_vname = "url_to_check";
                s_vtype = Url;
                s_has_value = false;
                s_initial_value = S_Noexpr;
            }];
      s_body = []
  } map))



(* determine whether a variable exists in a scope (and checks parent scopes) *)
let rec find_variable (scope : s_symbol_table) name =
  try
    List.find (fun s -> s.s_vname = name) scope.variables
  with Not_found ->
    match scope.parent with
      Some(parent) -> find_variable parent name
    | _ -> raise Not_found

let rec confirm_variable_does_not_exist (scope : s_symbol_table) name =
  if (List.exists (fun s -> s.s_vname = name) scope.variables) then
    raise (Variable_Name_Already_Exists name)
  else  
    match scope.parent with
      Some(parent) -> confirm_variable_does_not_exist parent name
    | _ -> ()

let rec loc_var_exists vars name =
	if vars = [] then false 
	else if (List.hd vars).s_vname = name then true
	else (loc_var_exists (List.tl vars) name)
			
(* converts string to Sast type *)
let c_type = function
    "void" -> Void
  | "int" -> Int
  | "string" -> Str
  | "double" -> Double
  | "url" -> Url
  | "selector" -> Sel(false)
  | "boolean" -> Bool
  | "None" -> Void
  | "int[]" -> Arr(Int, None)
  | "string[]" -> Arr(Str, None)
  | "double[]" -> Arr(Double, None)
  | "url[]" -> Arr(Url, None)
  | "boolean[]" -> Arr(Bool, None)
  | "selector[]" -> Arr(Sel(false), None)
  | a -> raise (Type_Expected [a; ""])

let rec type_string = function
  	Int -> "int"
  | Double -> "double"
  | Str -> "string"
  | Url -> "url"
  | Bool -> "bool"
  | Void -> "void"
  | Sel(_) -> "Selector"
  | Elements -> "elements"
  | Arr(a, _) -> type_string a ^ "[]"
 
let is_array = function 
	Ast.Array(_,_) -> true
  | _ -> false
 
let is_s_array = function
	Arr(a, _ ) -> true
  | _ -> false

  
let rec name_extract = function
    Ast.Int(name, _ ) -> name
  | Ast.Double(name,_) -> name
  | Ast.Str(name,_) -> name
  | Ast.Url(name,_) -> name
  | Ast.Sel(name,_) -> name
  | Ast.Array(arr,_) -> name_extract arr
  | Ast.Bool(name,_) -> name

let rec exp_extract = function
    Ast.Int(_, expr ) -> expr
  | Ast.Double(_,expr) -> expr
  | Ast.Str(_,expr) -> expr
  | Ast.Url(_,expr) -> expr
  | Ast.Sel(_,expr) -> expr
  | Ast.Array(vdecl,_) -> exp_extract vdecl
  | Ast.Bool(_,expr) -> expr
  
let str_id = function
	Ast.Id(name) -> name
  | _ -> "ERROR"

let u_arr_type = function
	Arr(t,_) -> t
  | _ -> Void
  
let rec array_size_extract = function
    Ast.Array(_,size) -> size
  | _ -> 0

let some_size = function 
	Some(size) -> size
  | _ -> 0
  
let requireArith op = 
	let op_type = (type_of_op op) in
	if op_type = "Arithmetic" then
		op
	else
		raise (Invalid_Operation op_type)
		
let loosely_typed t1 t2 = 
	let type1 = type_string t1 in
	let type2 = type_string t2 in
	if(type1="int" || type1="double") && (type2="int" || type2="double") then true
	else false
	
let is_initList = function
	S_ArrayLiteral(expr_list, size) -> true
  | _ -> false
  
(* Validate that arguments being passed to a function are consistant with the function's
   formals. *)
let rec checkArgs fname formals args exps =
	if List.length formals <> List.length args && (not ((fname = "print" || fname = "println") && (List.length args) = 0)) then
		raise (Invalid_Arguments ("Incorrect number of arguments for " ^ fname ^ ". Found " ^ string_of_int(List.length args) ^ ". Expected " ^ string_of_int(List.length formals) ^ "."))
	else if formals = [] && args = [] then 
		exps
	else if ((fname = "print" || fname = "println") && (List.length args) = 0)
		then [S_StringLiteral("")]
	else
		let (e, y) = (List.hd args) in
		let init_id = function 
			S_Id(name,init) -> if init then "" else name
		 |  _ -> "" in
		(if (init_id e) <> "" then raise(Variable_Not_Initalized (init_id e))		
		else if (is_initList e) then
			raise(Invalid_Init_List_Arg)
		else if(type_string (List.hd formals).s_vtype) = (type_string y) then 
			(checkArgs fname (List.tl formals) (List.tl args) (e :: exps))
     (* Print and println are the only two overloaded functions. They can accept any type. *)
		 else if ((fname = "print" || fname = "println") && (y <> Void)) then 
			(checkArgs fname (List.tl formals) (List.tl args) (e :: exps))
		 else 
			raise(Invalid_Arguments ((type_string y) ^ " instead of " ^ (type_string (List.hd formals).s_vtype) ^ " " ^ ((List.hd formals).s_vname))))
		 

 
let get_size = function
	S_ArrayLiteral(expr_list, size) -> size
  | _ -> 0

let rec initList_size expr_list count =
	if expr_list = [] then 0
	else initList_size (List.tl expr_list) count+1

(* extract type from VDecl *)
let rec type_extract env scope exp = 
  match exp with
    Ast.Int(_,_) -> Int
  | Ast.Double(_,_) -> Double
  | Ast.Str(_,_) -> Str
  | Ast.Array(t,s) -> Arr(type_extract env scope t, Some(s))
  | Ast.Url(_,_) -> Url
  | Ast.Sel(_,s) -> 
      let has_attr = get_selector_exp_has_attr env scope s in
        Sel(has_attr)
  | Ast.Bool(_,_) -> Bool

(* takes an expression (that should evaluate to a Sel()) and returns whether it is a Sel(true) or Sel(false). *)
and get_selector_exp_has_attr env scope selector_exp =
  match c_expr env scope selector_exp with 
      (_, Sel(i)) -> i

      (* rule: when selector is declared, it must be initialized to a type immediately *)
    | (S_Noexpr, _) -> raise (Expression_Expected "A selector must be initialized with a value immediately upon declaration.")
    | _ -> raise (Mismatched_Types "Expected selector")

and handle_list_valued_params env scope str expr_list =
    raise Not_Implemented

and c_expr env scope exp =
  match exp with
    IntLiteral(int_val) -> S_IntLiteral(int_val), Int
  | StringLiteral(str) -> S_StringLiteral(str), Str
  | DoubleLiteral(db_val) -> S_DoubleLiteral(db_val), Double
  | ArrayLiteral(expr_list) -> (*S_ArrayLiteral([]), Void *)
	  if expr_list = [] then S_ArrayLiteral([], 0), Arr(Void, None) else 
      let ele = c_expr env scope (List.hd expr_list) in
      let (ex, ty) = ele in
      let same_type = (fun exp -> 
        let (e, t) = (c_expr env scope exp) in
        if(t = ty) then e
        else raise(Mismatched_Types (type_string ty ^ " " ^ type_string t))) in
      S_ArrayLiteral(List.map same_type expr_list, (initList_size expr_list 0) ), Arr(ty, None)
  | BoolLiteral(bool_value) -> S_BoolLiteral(bool_value), Bool
  | Id(str) ->  let vdecl = try
                 find_variable scope str (* locate a variable by name *) 
                with Not_found ->
                  raise (Undeclared_Identifier str) (*(String.concat "" (List.map (fun s -> s.s_vname) scope.variables)))*)
                in
                let typ = vdecl.s_vtype in (* get the variable's type *)
                S_Id(vdecl.s_vname, vdecl.s_has_value), typ
  | ArrayAccess(id, index) -> 
      let arr = 
        try find_variable scope id
        with Not_found -> raise(Undeclared_Identifier id) in
      (match arr.s_vtype with
        Arr(atype, Some(_)) -> S_ArrayAccess(id, index), atype
      | _ -> raise (Index_of_NonArray id))
      
  | Binop(e1,op,e2) -> 
      let (e1, t1) = (c_expr env scope e1) in 
      let (e2, t2) = (c_expr env scope e2) in
      
      (* Handle selectors.  Idea: Selectors can be applied like: :"myurl" * <<div.banner img@src>> 
          ... using asterisk to apply selector to url. *)
      if op = Ast.Mult && (t1 = Url || t1 = Elements) && t2 = Sel(false) then
        S_ApplySelector(false, e1, e2), Elements
      else if op = Ast.Mult && (t1 = Url || t1 = Elements) && t2 = Sel(true) then
        S_ApplySelector(true, e1, e2), Arr(Str, None)
      else
        (if (match (t1, t2) with (Arr(a,_), Arr(b,_)) -> (if a = b then true else false) | _ -> false) then
          (env.has_array_concat <- true; S_ArrayConcat(e1, e2), t1)
		  
		 else if t1=t2 then 
    		(if (t1 = Int || t1 = Double) then 
    			(if type_of_op op = "Arithmetic" then
    				S_Binop(e1,op,e2), t1
    			 else
    				S_Binop(e1,op,e2), Bool)
    		else if t1 = Str && op=Ast.Add then
    			S_Binop(e1,op,e2), Str
    		else
    			raise(Invalid_Operation (type_string t1)))
        else if (loosely_typed t1 t2) then
        	S_Binop(e1,op,e2), Double
        else
        	raise(Mismatched_Types (type_string t1 ^ " " ^ type_string t2)))
  | Assign(id,expr) -> 
	  let var_exists = try find_variable scope id
        with Not_found -> 
          raise(Undeclared_Identifier id)
      in 
	  (var_exists.s_has_value <- true);
      (let (e, ty ) = (c_expr env scope expr) in
	  if (is_initList e) then raise(Initalizer_List_On_NonArray id)
	  else if (type_string (var_exists.s_vtype)) <> (type_string ty) then raise(Type_Expected [(type_string (var_exists.s_vtype)); (type_string ty)])
      else S_Assign(id, e), var_exists.s_vtype)
  | ArrayAssign(id, index, expr) -> 
  	  let var = try find_variable scope id with Not_found -> raise(Undeclared_Identifier id) in
      let (e, _ ) = (c_expr env scope expr) in
      (match var.s_vtype with
          Arr(atype, _) -> S_ArrayAssign(id, index, e), atype
        | _ -> raise(Index_of_NonArray (string_of_int index)))
  | AssignOp(str1,op,expr) -> 
      let (e, _ ) = (c_expr env scope expr) in
        S_AssignOp(str1, requireArith(op), e), Int 
  | DoubleOp(str1,op,expr) -> 
      let (e, _ ) = (c_expr env scope expr) in
        S_DoubleOp(str1, requireArith(op), e), Int 
  | Call(str,expr_list) -> 
  		if StringMap.mem str (include_built_in_functions env.functions) then
  			let f = (StringMap.find str (include_built_in_functions env.functions)) in
  			let f_name = f.s_fname in
  			let f_type = f.s_return_type in
  			let f_args = f.s_formals in
  			let list_check = (fun ex -> (c_expr env scope ex)) in
			let is_arg_array arg = (match c_expr env scope arg with (_,Arr(_,_)) -> true | _ -> false) in
          if (str = "print" || str = "println") && ((List.length expr_list) = 1) && (is_arg_array (List.hd expr_list)) then
            let (arg_expr,_) = c_expr env scope (List.hd expr_list) in
              (S_PrintArray(arg_expr), Void)
          else if str = "live" && ((List.length expr_list) = 1) && (is_arg_array (List.hd expr_list)) then
            let (arg_expr,_) = c_expr env scope (List.hd expr_list) in
              (S_Call("live", [arg_expr]), Arr(Url, None))
          else
    			  S_Call(str, List.rev (checkArgs f_name f_args (List.map list_check expr_list) [])), f_type 
  		else
  			raise (Function_Not_Found str)
  | Ast.Selector(elem_selector_list,attr_selector) -> 
      env.has_selectors <- true;
      let ret_type = (if attr_selector = NoAttr then Sel(false) else Sel(true)) in
        S_Selector(elem_selector_list,attr_selector), ret_type
  | UrlConstructor(expr) -> 
      env.has_selectors <- true;
  		let s_exp = c_expr env scope expr in
  		let (e, ty) = s_exp in
    		if ty = Str then
    			S_UrlConstructor(e), Url
    		else
    			raise (Type_Expected ["url"; (type_string ty)])
  | Noexpr -> S_Noexpr, Void

let get_vdecl env scope id initial (vartype : t) =
    (confirm_variable_does_not_exist scope id);
    let initial_val = c_expr env scope initial in
    let (initial_val_expr, initial_val_type) = initial_val in
        { s_vname = id; 
          s_vtype = vartype; 
          s_initial_value = (if (((type_string initial_val_type) <> (type_string vartype)) 
								&& initial_val_expr <> S_Noexpr 
								&& (not ((is_s_array vartype) && ((type_string initial_val_type) = "void[]")))) then
								raise(Type_Expected [(type_string vartype); (type_string initial_val_type)]) else initial_val_expr); 
          s_has_value = (initial_val_expr <> S_Noexpr);
          }

let c_vdecl env scope v =
  match v with
    Ast.Int(id, initial) -> 
      get_vdecl env scope id initial Int
  | Ast.Str(id, initial) -> 
      get_vdecl env scope id initial Str
  | Ast.Double(id, initial) -> 
      get_vdecl env scope id initial Double
  | Ast.Url(id, initial) -> 
      get_vdecl env scope id initial Url
  | Ast.Bool(id, initial) ->
      get_vdecl env scope id initial Bool
  | Ast.Sel(id, initial) ->
      get_vdecl env scope id initial (Sel(get_selector_exp_has_attr env scope initial))
  | Ast.Array(vartype, size) -> 
      (match vartype with
        Ast.Int(id, initial) -> 
          (confirm_variable_does_not_exist scope id);
          get_vdecl env scope id initial (Arr(Int, Some(size)))
      | Ast.Str(id, initial) -> 
          (confirm_variable_does_not_exist scope id);
          get_vdecl env scope id initial (Arr(Str, Some(size)))
      | Ast.Double(id, initial) -> 
          (confirm_variable_does_not_exist scope id);
          get_vdecl env scope id initial (Arr(Double, Some(size)))
      | Ast.Url(id, initial) -> 
          (confirm_variable_does_not_exist scope id);
          get_vdecl env scope id initial (Arr(Url, Some(size)))
      | Ast.Sel(id, initial) -> 
          (confirm_variable_does_not_exist scope id);
          get_vdecl env scope id initial (Arr(Sel(get_selector_exp_has_attr env scope initial), Some(size)))
      | Ast.Array(_,_) -> raise Array_Of_Arrays_Not_Allowed
      | Ast.Bool (id, initial) ->
          (confirm_variable_does_not_exist scope id);
          get_vdecl env scope id initial (Arr(Bool, Some(size))))

let is_return = function 
	S_Return(_,_) -> true 
  | _ -> false
		  
let get_return_type = function
	S_Return(_,rtype) -> rtype
  | _ -> Void
		  
let rec c_stmt_list env scope sl = 
  List.map (fun a -> c_stmt env scope a) sl
and c_stmt env scope s =
  match s with 
    Block(stmt_list) -> 
      let inner_scope = { parent = Some scope; variables = [] } in
        S_Block(c_stmt_list env inner_scope stmt_list)
  | Vdecl(vdec) ->
      let decl = c_vdecl env scope vdec in
      scope.variables <- (decl :: scope.variables);

      S_Vdecl(decl)
  | Expr(exp) -> 
      let (e1,_) = c_expr env scope exp in
        S_Expr(e1)
  | Return(ex) ->
      (* TODO: Check return type *)
      let (e1,ex_type) = c_expr env scope ex in
        S_Return(e1, ex_type)
  | If(ex,st1,st2) -> 
      let (e1,ex_type) = c_expr env scope ex in
		if ex_type = Bool then 
			S_If(e1 ,c_stmt env scope st1, c_stmt env scope st2)
		else
			raise (Type_Expected ["boolean"; (type_string ex_type)])
  | For(vartype,id,expr1,expr2,expr3,stmt) ->
	  let inner_scope = { parent = Some scope; variables = [] } in
	  (if vartype <> "None" then 
      inner_scope.variables <- 
        { s_vname = (str_id id); 
          s_vtype = (c_type vartype);
          s_has_value = true;
          s_initial_value = S_Noexpr; } :: inner_scope.variables);
      let (expr_val1, expr_type1) = c_expr env inner_scope expr1 in
      let (expr_val2, expr_type2) = c_expr env inner_scope expr2 in
      let (expr_val3, expr_type3) = c_expr env inner_scope expr3 in
      let vtype = c_type vartype in
        if expr_type1 != Int then
          raise (Invalid_Operation "First expression in for loop must be an integer declaration.")
        else if expr_type2 != Bool then
          raise (Invalid_Operation "Second expression in for loop must be boolean test.")
        else if expr_type3 != Int then
          raise (Invalid_Operation "Third expression in for loop must increment.")
        else
          S_For(vtype, expr_val1, expr_val2, expr_val3, c_stmt env inner_scope stmt)

  | While(expr,stmt) -> 
	  let inner_scope = { parent = Some scope; variables = [] } in
      let (expr_val,expr_type) = c_expr env inner_scope expr in
        S_While(expr_val,c_stmt env inner_scope stmt)
  | Loop(arr, target, stmt) ->
	  let decl = (find_variable scope arr) in 
	  (if (not (is_s_array decl.s_vtype)) then raise(LoopRequiresArrayTarget arr) else
		  (let inner_scope = { parent = Some scope; variables = [{ s_vname = target; s_vtype = (u_arr_type decl.s_vtype); s_has_value = true; s_initial_value = S_Noexpr; }] } in
		  S_Loop(arr, { s_vname = target; s_vtype = (u_arr_type decl.s_vtype); s_has_value = true; s_initial_value = S_Noexpr; }, c_stmt env inner_scope stmt)))
  | Nostmt -> S_Nostmt

let c_func_def env (b : Ast.func_decl) = 
  if StringMap.mem b.fname env.functions then
    raise (Function_Name_Already_Exists b.fname)
  else
    let formals = 
      if b.fname = "main" then 
        Ast.Array(Ast.Str("args", Noexpr), 1) :: b.formals
      else 
        b.formals in
    env.functions <- StringMap.add b.fname 
      { s_fname = b.fname; 
        s_return_type = (c_type b.ftype); 
        s_formals = (List.map (fun vd -> 
          { s_vname = (name_extract vd); 
            s_vtype = (type_extract env env.scope vd);
            s_has_value = false;
            s_initial_value = S_Noexpr;
          }) formals);
        s_body = []
      } env.functions	  
	  
let c_func_body env (b : Ast.func_decl)= 
	let formals = 
		  if b.fname = "main" then 
			Ast.Array(Ast.Str("args", Noexpr), 1) :: b.formals
		  else 
			b.formals in
  let local_scope = { 
                    parent = Some env.scope; 
                    variables = (List.map (fun vd -> 
								  { s_vname = (name_extract vd); 
									s_vtype = (type_extract env env.scope vd);
									s_has_value = true;
									s_initial_value = S_Noexpr;
								  }) formals)  
                  } in
    if not (StringMap.mem b.fname env.functions) then
      raise (Function_Not_Found b.fname)
    else
      let f = StringMap.find b.fname env.functions in
	  let f_return = f.s_return_type in
	  let f_body = c_stmt_list env local_scope b.body in
	  let check_return = (fun stmt -> if (is_return stmt) then 
				(if (type_string f_return = "void") then 
					raise(Expected_No_Return)
				 else if (type_string f_return) <> (type_string (get_return_type stmt)) then 
					raise(Invalid_Return_Type [(type_string f_return); (type_string (get_return_type stmt))])
				 else stmt) else stmt) in
		(f.s_body <- (List.map check_return f_body))
		

let rec v_decl_to_s_decl v_decl env = 
	match v_decl with
		Ast.Int(id, expr) ->  { 
							s_vname = id; 
							s_vtype = Int; 
							s_has_value = (expr <> Ast.Noexpr); 
							s_initial_value = (if expr = Ast.Noexpr then S_Noexpr else let (s, _) = (c_expr env env.scope expr) in s);
						  }
	  | Ast.Bool(id, expr) -> { 
							s_vname = id; 
							s_vtype = Bool; 
							s_has_value = (expr <> Ast.Noexpr); 
							s_initial_value = (if expr = Ast.Noexpr then S_Noexpr else let (s, _) = (c_expr env env.scope expr) in s);
						  }
      | Ast.Str(id, expr) -> { 
							s_vname = id; 
							s_vtype = Str; 
							s_has_value = (expr <> Ast.Noexpr); 
							s_initial_value = (if expr = Ast.Noexpr then S_Noexpr else let (s, _) = (c_expr env env.scope expr) in s);
						  }
      | Ast.Double(id, expr) -> { 
							s_vname = id; 
							s_vtype = Double; 
							s_has_value = (expr <> Ast.Noexpr); 
							s_initial_value = (if expr = Ast.Noexpr then S_Noexpr else let (s, _) = (c_expr env env.scope expr) in s);
						  }
      | Ast.Url(id, expr) -> { 
							s_vname = id; 
							s_vtype = Url; 
							s_has_value = (expr <> Ast.Noexpr); 
							s_initial_value = (if expr = Ast.Noexpr then S_Noexpr else let (s, _) = (c_expr env env.scope expr) in s);
						  }
      | Ast.Sel(id, expr) -> { 
							s_vname = id; 
							s_vtype = (Sel(get_selector_exp_has_attr env env.scope expr)); 
							s_has_value = (expr <> Ast.Noexpr); 
							s_initial_value = (if expr = Ast.Noexpr then S_Noexpr else let (s, _) = (c_expr env env.scope expr) in s);
						  }
      | Ast.Array(vdecl, size) -> { 
							s_vname = (name_extract vdecl); 
							s_vtype = Arr((type_extract env env.scope vdecl), Some(size)); 
							s_has_value = ((exp_extract vdecl) <> Ast.Noexpr); 
							s_initial_value = (if (exp_extract vdecl) = Ast.Noexpr then S_Noexpr else let (s, _) = (c_expr env env.scope (exp_extract vdecl)) in s);
						  }
		
let c_glob_var env a =
  if loc_var_exists env.scope.variables (name_extract a) then
    raise (Variable_Name_Already_Exists (name_extract a))
  else 
	env.scope.variables <- ((v_decl_to_s_decl a env) :: env.scope.variables)

(* Convert an Ast to an Sast. Throws exceptions for type errors. *)		
let compile (vars, (funcs : Ast.func_decl list)) =
  let env = { functions = StringMap.empty; scope = { parent = None; variables = [] }; has_selectors = false; has_array_concat = false; } in
    (*Declare the main function even before starting to parse through global
      variables.  Global variables may have initial instantiations, which will
      need to be added to beginning of the main function. *)
    List.iter (fun a -> c_glob_var env a) vars; (* add globals data into SAST environment *)
    List.iter (fun a -> c_func_def env a) funcs;  (* add function declarations into environment *)
    List.iter (fun a -> c_func_body env a) funcs;  (* add function bodies into functions *)
    env
