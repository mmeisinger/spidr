(* This file takes prints out an SAST to a Java file.  The main function
   is named 'print_program', takes an SAST environment, and returns a string. *)

open Sast

let rec string_of_type = function
  Void -> "void"
| Bool -> "boolean"
| Int -> "int"
| Str -> "String"
| Double -> "double"
| Url -> "SUrl"
| Sel(_) -> "SSelector"
| Elements -> "Element[]"
| Arr(a,_) -> string_of_type a ^ "[]"


let string_of_op = function
    Ast.Add -> "+"
  | Ast.Sub -> "-"
  | Ast.Mult -> "*"
  | Ast.Div -> "/"
  | Ast.Equal -> "=="
  | Ast.Neq -> "!="
  | Ast.Less -> "<"
  | Ast.Leq -> "<="
  | Ast.Greater -> ">"
  | Ast.Geq -> ">="

let print_formal f =
  string_of_type f.s_vtype ^ " " ^ f.s_vname

let print_elem_sel es =
  let Ast.ElemSelector(a,b,c,d) = es in
  "new SSelector(\"" ^ a ^ "\",\"" ^ b ^ "\",\"" ^ c ^ "\",\"" ^ d ^ "\")"

let rec print_elem_sel_list l = 
  match l with
    a :: b when (List.length b = 0) -> print_elem_sel a
  | a :: b -> "combineSelectors(" ^ print_elem_sel_list b ^ "," ^ print_elem_sel a ^ ")"
  | _ -> ""

let print_att_sel a =
  "new SAttSelector(\"" ^ a ^ "\")"

let rec print_expr = function
  S_IntLiteral(i) -> string_of_int i
| S_StringLiteral(str) -> "\"" ^ str ^ "\""
| S_DoubleLiteral(d) -> string_of_float d
| S_ArrayLiteral(arr, size) -> 
    if List.length arr > 0 then
      "app.array(" ^ (String.concat "," (List.map print_expr arr)) ^ ")"
    else
      "{}"
| S_BoolLiteral(b) -> string_of_bool b
| S_Id(s,b) -> s
| S_Binop(s_expr1,op,s_expr2) -> 
	print_expr s_expr1 ^ (string_of_op op) ^ print_expr s_expr2
| S_Assign(id,e) -> id ^ " = " ^ print_expr e
| S_AssignOp(s,op,s_expr) -> s ^ "=" ^ s ^ string_of_op op ^ (print_expr s_expr) 
| S_DoubleOp(id, sign, e) -> id ^ (match sign with Ast.Add -> "++" | Ast.Sub -> "--" | _ -> "")
| S_Call(f_name,exp_list) -> 
    if f_name = "print" then
      ("System.out.print(" ^ print_expr (List.hd exp_list) ^ ")")
    else if f_name = "println" then
      "System.out.println(" ^ print_expr (List.hd exp_list) ^ ")"
    else if f_name = "live" then
      "live(" ^ print_expr (List.hd exp_list) ^ ")"
    else
      "app." ^ f_name ^ "(" ^ (String.concat "," (List.map print_expr exp_list)) ^ ")"
| S_PrintArray(exp) ->
    "System.out.print(java.util.Arrays.toString(" ^ print_expr exp ^ "))"
| S_Selector(e_l,a) ->
    (match a with 
      Ast.NoAttr -> print_elem_sel_list e_l
    | Ast.AttrSelector(att) -> "combineSelectors(" ^ print_elem_sel_list e_l ^ "," ^ print_att_sel att ^ ")")
| S_UrlConstructor(exp) -> 
    "new SUrl(" ^ print_expr exp ^ ")"
| S_ArrayAssign(id,index,expr) ->  id ^ "[" ^ string_of_int index ^ "]=" ^ (print_expr expr)
| S_ArrayAccess(id,i) -> id ^ "[" ^ string_of_int i ^ "]"
| S_ArrayConcat(a,b) -> "arrayConcat(" ^ print_expr a ^ "," ^ print_expr b ^ ")"
| S_ApplySelector(return_string_array, url, sel) -> 
    if (return_string_array) then
      "applyAttSelector(" ^ print_expr url ^ "," ^ print_expr sel ^ ")"
    else
      "getElementsMatchingSelector(" ^ print_expr url ^ "," ^ print_expr sel ^ ")"
| S_Noexpr -> ""

let print_v_decl v =
  string_of_type v.s_vtype ^ " " ^ v.s_vname
  ^ (match v.s_initial_value with
        S_Noexpr -> ""
      | exp -> " = " ^ print_expr exp)

let rec print_stmt_list body =
  String.concat "\n" (List.map print_stmt body)
and print_stmt = function
  S_Block(stmt_l) -> 
      "{\n"
      ^ print_stmt_list stmt_l
      ^ "\n}"
| S_Vdecl(v) -> print_v_decl v ^ ";"
| S_Expr(e) -> print_expr e ^ ";"
| S_Return(e, rtype) -> "return " ^ print_expr e ^ ";"
| S_If(e, s, S_Block([])) -> "if (" ^ print_expr e ^ ") " ^ print_stmt s
| S_If(e, s1, s2) ->  "if (" ^ print_expr e ^ ")\n" ^ print_stmt s1 ^ "\nelse " ^ print_stmt s2
| S_For(typ,s_expr1,s_expr2,s_expr3,s_stmt) -> "for("^ (let ty = string_of_type typ in if ty <> "void" then ty ^ " " else "") ^ print_expr s_expr1 ^ "; " ^ print_expr s_expr2 ^ "; " ^ print_expr s_expr3 ^ ")" ^ print_stmt s_stmt
| S_While(s_expr,s_stmt) -> "while(" ^ print_expr s_expr ^ ")" ^ print_stmt s_stmt 
| S_Loop(arr, target, stmt) -> "for(" ^ (string_of_type target.s_vtype) ^ " " ^ target.s_vname ^ " : " ^ arr ^ ")" ^ print_stmt stmt
| S_Nostmt -> ""

let print_func_decl f env =
  let main_surround_try body =
    if f.s_fname = "main" && env.has_selectors then
      Jhelpers.main_try ^ body ^ Jhelpers.main_catch
    else
      body in
    "public static " ^ (string_of_type f.s_return_type) ^ " " ^ f.s_fname 
    ^ "("
    ^ String.concat ", " (List.map print_formal f.s_formals) 
    ^ ") " ^ (if env.has_selectors then "throws Exception " else "") ^ "{\n"
    ^ main_surround_try (print_stmt_list f.s_body)
    ^ "\n}\n"

let print_program (a : Sast.s_env_def) =
      (if a.has_selectors = true then Jhelpers.imports else "")
    ^ "public class app {\n"
	^ (let globals = (String.concat ";\n" (List.map print_v_decl a.scope.variables)) in if globals = "" then "" else globals ^ ";\n")
    ^ (if a.has_selectors = true then Jhelpers.instantiate_app else "")
    ^ String.concat "" (List.map (fun (_,f) -> print_func_decl f a) (StringMap.bindings a.functions))
    ^ (if a.has_selectors = true then Jhelpers.selector_functions else "")
	^ (if a.has_array_concat = true then Jhelpers.array_concat else "")
	^ Jhelpers.array_init
    ^ "\n}"
