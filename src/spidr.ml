open Printer

type action = Ast | Compile | Execute

(* Returns strings representing errors *)
let get_error_message = function
    Sast.Function_Name_Already_Exists(e) ->
      "Error: Function_Name_Already_Exists '" ^ e ^ "'."
  | Sast.Variable_Name_Already_Exists(e) ->
      "Error: Variable_Name_Already_Exists '" ^ e ^ "'."
  | Sast.Array_Of_Arrays_Not_Allowed ->
      "Error: Array_Of_Arrays_Not_Allowed"
  | Sast.Not_Implemented ->
      "Error: Not_Implemented"
  | Sast.Type_Expected(e) ->
      "Error: Type_Expected. Expected '" ^ (List.hd e) ^ "' instead found '" ^ (List.hd (List.tl e)) ^ "'."
  | Sast.Invalid_Operation(e) ->
      "Error: Invalid_Operation. " ^ e
  | Sast.Function_Not_Found(e) ->
      "Error: Function_Not_Found. " ^ e
  | Sast.Index_of_NonArray(e) ->
      "Error: Index_of_NonArray. " ^ e
  | Sast.Array_Not_Initialized(e) ->
      "Error: Array_Not_Initialized. " ^ e
  | Sast.Undeclared_Identifier(e) ->
      "Error: Variable '" ^ e ^ "' not declared."
  | Sast.Invalid_Init_List_Arg ->
      "Error: Initializer List cannot be passed as the argument of a function."
  | Sast.Expected_No_Return ->
      "Error: Return statement found in void function."
  | Sast.Invalid_Return_Type(e) ->
      "Error: Mismatched Return Type. Expected " ^ (List.hd e) ^ " but found " ^ (List.hd (List.tl e)) ^ "."
  | Sast.Expression_Expected(e) ->
      "Error: Expression_Expected. " ^ e
  | Sast.LoopRequiresArrayTarget(e) ->
      "Error: Variable '" ^ e ^ "' must be an array."
  | Sast.Variable_Not_Initalized(e) ->
      "Error: Variable '" ^ e ^ "' has not been initialized."
  | e -> 
      Printexc.to_string e

let _ =
  let action = if Array.length Sys.argv > 1 then
    List.assoc Sys.argv.(1) 
        [ ("-a", Ast);  (* Print Abstract Syntax Tree (just for debugging) *)
          ("-s", Compile); (* Compile Java using SAST *)
          ("-e", Execute)] (* Compile using SAST then execute immediately *)
  else Ast in
  try
    let lexbuf = Lexing.from_channel stdin in
    let program = Parser.program Scanner.token lexbuf in
      match action with

      (* For initial testing only -- Print primitive java *)
        Ast -> let listing = Ast.string_of_program program in
                  print_string listing 
      
      (* Compile to Java *)
      | Compile ->
          ( try
              let sast = Sast.compile program in
              let listing = Printer.print_program sast in
                      print_string listing
            with 
              e -> print_string (get_error_message e ^ "\n")
          )

      (* Compile to Java, package into executable Jar and execute *)
      | Execute ->
          ( try
              let sast = Sast.compile program in
              let listing = Printer.print_program sast in

              (* print the java file *)
              let oc = open_out "app.java" in
                Printf.fprintf oc "%s\n" listing;
                close_out oc;

                (* compile the java file *)
                ignore (Sys.command "javac -cp spidr-package.jar app.java");

                (* put it into the package *)
                ignore (Sys.command "jar uf spidr-package.jar app.class");

                (* run the file *)
                ignore (Sys.command "java -jar spidr-package.jar")
            with 
              e -> print_string (get_error_message e ^ "\n")
          )
          
  with
    e -> print_string (Printexc.to_string e ^ "\n")