#Makefile to compile java jar with helper libraries, and compile Spidr executable from ocaml

SOURCE =./java/SSelector.java\
	./java/SAttSelector.java\
	./java/SUrl.java
RES=./spidr-package.jar

default : $(SOURCE)
	cd src; make
	cd java; make
	cp java/spidr-package.jar spidr-package.jar
	cp src/spidr spidr

.PHONY : clean
clean :
	cd src; make clean
	cd java; make clean
	rm -f spidr-package.jar spidr app.java *.log *.class *.out *.diff

.PHONY : test
test : default testall.sh
	./testall.sh

.PHONY : testexe
testexe : default testall.sh
	./testall.sh -e