#!/bin/sh

SPIDR="./spidr"
JAVA="javac"

# Set time limit for all operations
ulimit -t 30

globallog=testall.log
rm -f $globallog
error=0
globalerror=0

RED='\033[01;31m' # red text
GREEN='\033[01;32m' # green text
RESET='\033[00;00m' # normal white

keep=0
exeoption=0
step=""

restotal=0
ressucceededjava=0
restotalexe=0
ressucceededexe=0
resskippedexe=0

Usage() {
    echo "Usage: testall.sh [options] [.spidr files]"
    echo "-k    Keep intermediate files"
    echo "-h    Print this help"
    exit 1
}

SignalError() {
    if [ $error -eq 0 ] ; then
	echo "${RED}Failed ${step} ${RESET}"
	error=1
    fi
    echo "  $1"
}

# Compare <outfile> <reffile> <difffile>
# Compares the outfile with reffile.  Differences, if any, written to difffile
Compare() {
    generatedfiles="$generatedfiles $3"
    echo diff -b $1 $2 ">" $3 1>&2
    diff -b "$1" "$2" > "$3" 2>&1 || {
        SignalError "$1 differs"
        echo "${RED}Failed ${step} ${RESET} $1 differs from $2" 1>&2
    }
}

# Run <args>
# Report the command, run it, and report any errors
Run() {
    echo $* 1>&2
    eval $* || {
	SignalError "$1 failed on $*"
	return 1
    }
}

Check() {
    error=0
    basename=`echo $1 | sed 's/.*\\///
                             s/.spidr//'`
    reffile=`echo $1 | sed 's/.spidr$//'`
    basedir="`echo $1 | sed 's/\/[^\/]*$//'`/."

    echo ""
    printf "%-45s" $basename
    #echo -n "$basename..."

    echo 1>&2
    echo "###### Testing $basename" 1>&2

    generatedfiles=""

    # Uncomment this to print out all Abstract Syntax Trees
    # Run "$SPIDR" "-a" "<" $1 ">" ${basename}.a.out &&
    # generatedfiles="$generatedfiles ${basename}.a.out" &&

    # GENERATE JAVA
    
    restotal=$((restotal+1))

    step="Java"

    generatedfiles="$generatedfiles ${basename}.s.out" &&
    Run "$SPIDR" "-s" "<" $1 ">" ${basename}.s.out &&
    Compare ${basename}.s.out ${reffile}.java ${basename}.s.diff

    if [ $error -eq 0 ] ; then
        ressucceededjava=$((ressucceededjava+1))
        run "rm -f $generatedfiles"
        echo -n "${GREEN}Passed ${step}${RESET}  "
        echo "###### ${GREEN}Success ${RESET}" 1>&2

        # EXECUTE JAVA (only execute if -exe option is set)
        if [ $exeoption -eq 1 ] ; then
            
            step="Execute"

            if [ -f "${reffile}.out" ] ; then

                restotalexe=$((restotalexe+1))

                generatedfiles="$generatedfiles ${basename}.e.out" &&
                # Copy java file to try executing it
                #Run "cp" ${basename}.s.out "app.java" &&
                # Compile it, referencing the jsoup library
                #Run "javac -cp jsoup-1.7.1.jar app.java" 1>&2 &&
                # Builds an executable jar file.  The manifest allows it to be executed.
                #Run "jar cmfv manifest.mf app.jar *.class" 1>&2 &&
                # Run the executable jar file
                #Run "java -jar app.jar >" ${basename}.e.out >&2 &&

                Run "$SPIDR" "-e" "<" $1 ">" ${basename}.e.out &&
                Compare ${basename}.e.out ${reffile}.out ${basename}.e.diff

                if [ $error -eq 0 ] ; then
                    ressucceededexe=$((ressucceededexe+1))
                    run "rm -f $generatedfiles"
                    echo -n "${GREEN}Passed ${step}${RESET}"
                    echo "###### ${GREEN}Success ${RESET}" 1>&2
                else
                    echo "###### ${RED}Failed ${step} ${RESET}" 1>&2
                fi

            else
                    echo -n "${GREEN}Skipped ${step}${RESET}"
                    resskippedexe=$((resskippedexe+1))
            fi
        fi

    else
        echo "###### ${RED}Failed ${step} ${RESET}" 1>&2
    fi

    # Report the status and clean up the generated files

    if [ $error -eq 0 ] ; then
    	if [ $keep -eq 0 ] ; then
    	    rm -f $generatedfiles
    	fi

    	echo "###### ${GREEN}Success ${RESET}" 1>&2
    else
    	echo "###### ${RED}Failed ${step} ${RESET}" 1>&2
    	globalerror=$error
    fi
}

while getopts kdpshe c; do
    case $c in
	k) # Keep intermediate files
	    keep=1
	    ;;
    e)
        exeoption=1
        ;;
	h) # Help
	    Usage
	    ;;
    esac
done

shift `expr $OPTIND - 1`

if [ $# -ge 1 ]
then
    files=$@
else
    files="tests/fail-*.spidr tests/test-*.spidr"
fi

for file in $files
do
    case $file in
	*test-*)
	    Check $file 2>> $globallog
	    ;;
	*fail-*)
	    CheckFail $file 2>> $globallog
	    ;;
	*)
	    echo "unknown file type $file"
	    globalerror=1
	    ;;
    esac
done

echo ""
echo "${ressucceededjava} of ${restotal} passed Java generation tests"

if [ $exeoption -eq 1 ] ; then
    echo "${ressucceededexe} of ${restotalexe} passed execution output comparison tests"
    echo "${resskippedexe} execution tests skipped (no *.out file associated with test)"
fi

exit $globalerror
