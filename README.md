# Spidr is a project for Columbia University class PLT in Fall 2012

Prerequisites:
	javac
	ocaml

In order to use Spidr, you will need to make the compiler and java libraries, then execute the compiler on a *.spidr source file.

To make the spidr compiler:
	Run 'make' at the root of the unzipped spidr folders.

To execute a source file:
	Run './spidr -e < myfile.spidr' to compile and execute the spidr source file.


To run the test suite, type either of the following when in the root of the project:

	make test 
		Tests whether the java compiles into the expected format.

	make testexe 
		Tests whether the java compiles and whether when the java is run the output is correct.


'make clean' removes all unnecessary files from all of the spidr folders.