import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SSelector {
	public String elementName;
	public String className;
	public String attr;
	public String attrValue;
	public SSelector innerSelector;
	public SAttSelector attSelector;
	
	public SSelector(String elementName, String className, String attr, String attrValue) {
		this.elementName = elementName;
		this.className = className;
		this.attr = attr;
		this.attrValue = attrValue;
		this.innerSelector = null;
		this.attSelector = null;
	}
	
	public String toString(){
		return "<<" + this.elementName + (this.className.isEmpty() ? "" : "." + this.className + (this.attr.isEmpty() ? "" : ("[" + this.attr + (this.attrValue.isEmpty() ? "" : "=\"" + this.attrValue + "\"") + "]"))) + (this.attSelector == null ? "" : this.attSelector.toString()) + ">>";
	}
}
