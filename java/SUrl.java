import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SUrl {
	public String url;
	
	public SUrl(String url) {
		this.url = url;
	}
	public String toString(){
		return ":\"" + this.url + "\"";
	}
}
