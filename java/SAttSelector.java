import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SAttSelector {
	public String att;
	
	public SAttSelector(String att) {
		this.att = att;
	}
	
	public String toString(){
		return "@" + this.att;
	}
}
