public class app {
public static void main(String[] args) {
int[] list = app.array(1,2,3);
int[] list2 = app.array(4,5);
int[] list3 = arrayConcat(list,list2);
System.out.print(java.util.Arrays.toString(list3));
}


	public static String[] arrayConcat(String[] array1, String[] array2){
		String[] array3= new String[array1.length+array2.length];
		System.arraycopy(array1, 0, array3, 0, array1.length);
		System.arraycopy(array2, 0, array3, array1.length, array2.length);
		return array3;
	}
		
	public static int[] arrayConcat(int[] array1, int[] array2) {
		int[] array3= new int[array1.length+array2.length];
		System.arraycopy(array1, 0, array3, 0, array1.length);
		System.arraycopy(array2, 0, array3, array1.length, array2.length);
		return array3;
	}
	
	public static double[] arrayConcat(double[] array1, double[] array2){
		double[] array3= new double[array1.length+array2.length];
		System.arraycopy(array1, 0, array3, 0, array1.length);
		System.arraycopy(array2, 0, array3, array1.length, array2.length);
		return array3;
	}

	public static boolean[] arrayConcat(boolean[] array1, boolean[] array2){
		boolean[] array3= new boolean[array1.length+array2.length];
		System.arraycopy(array1, 0, array3, 0, array1.length);
		System.arraycopy(array2, 0, array3, array1.length, array2.length);
		return array3;
	}


	public static int[] array(int... values){
		return values;
	}
	public static String[] array(String... values){
		return values;
	}
	public static boolean[] array(boolean... values){
		return values;
	}

}