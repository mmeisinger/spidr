import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class app {
  public static app spidr_app = new app();
public static void main(String[] args) throws Exception {
		try {
SUrl testUrl = new SUrl("http://www.columbia.edu/");
System.out.print(testUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}
}


	
	public static SUrl[] array(SUrl... values){
		return values;
	}
	public static SSelector[] array(SSelector... values){
		return values;
	}
	public static SUrl[] arrayConcat(SUrl[] array1, SUrl[] array2){
		SUrl[] array3 = new SUrl[array1.length + array2.length];
		for(int i=0; i<array3.length; i++){
			if(i<array1.length)
				array3[i] = array1[i];
			else
				array3[i-array1.length] = array2[i-array1.length];
		}	
		return array3;
	}
	public static SSelector[] arrayConcat(SSelector[] array1, SSelector[] array2){
		SSelector[] array3 = new SSelector[array1.length + array2.length];
		for(int i=0; i<array3.length; i++){
			if(i<array1.length)
				array3[i] = array1[i];
			else
				array3[i-array1.length] = array2[i-array1.length];
		}	
		return array3;
	}
	
	private static String[] applyAttSelector(SUrl u, SSelector s) throws Exception {
		Element[] urlElements = Jsoup.connect(u.url).get().children().toArray(new Element[] {});
		return applyAttSelector(urlElements, s);
	}
	private static String[] applyAttSelector(Element[] sourceList, SSelector a) throws Exception {
		if (a.attSelector != null) {
			return applyAttSelector(getElementsMatchingSelector(sourceList, a), a.attSelector);
		}
		else if (a.innerSelector != null) {
			return applyAttSelector(sourceList, a.innerSelector);
		}
		else {
			throw new Exception("Internal error #1");
		}
	}
	private static String[] applyAttSelector(Element[] sourceList, SAttSelector a){
		List<String> ret = new ArrayList<String>();
		for (Element e : sourceList) {
			if (e.hasAttr(a.att)) {
				ret.add(e.attr(a.att));
			}
		}
		return ret.toArray(new String[] {});
	}
	private static SSelector combineSelectors(SSelector s1, SSelector s2){
		if (s1.innerSelector == null){
			s1.innerSelector = s2;
			return s1;
		}
		else {
			combineSelectors(s1.innerSelector, s2);
			return s1;
		}
	}
	private static SSelector combineSelectors(SSelector s1, SAttSelector s2) throws Exception{
		if (s1.attSelector != null)
			throw new Exception("This selector already has an attribute selector applied to it. Only one attribute selector may be applied per selector.");
		if (s1.innerSelector == null){
			s1.attSelector = s2;
			return s1;
		}
		else {
			combineSelectors(s1.innerSelector, s2);
			return s1;
		}
	}

	private static Element[] getElementsMatchingSelector(SUrl u, SSelector s) throws Exception {
		Element[] urlElements = Jsoup.connect(u.url).get().children().toArray(new Element[] {});
		return getElementsMatchingSelector(urlElements, s);
	}
	private static Element[] getElementsMatchingSelector(Element[] sourceList, SSelector s) throws Exception {
    	
		List<Element> ret = new ArrayList<Element>();
		for (Element e : sourceList) {
			boolean isMatching = true;
			if (!s.elementName.isEmpty() && e.tagName() != s.elementName) isMatching = false;
			if (!s.className.isEmpty() && !e.classNames().contains(s.className)) isMatching = false;
			if (!s.attr.isEmpty() && !e.hasAttr(s.attr)) isMatching = false;
			if (!s.attr.isEmpty() && !s.attrValue.isEmpty() && e.attr(s.attr) != s.attrValue) isMatching = false;
			
			if (isMatching && s.innerSelector != null) {
				Element[] matches = getElementsMatchingSelector(e.children().toArray(new Element[] {}), s.innerSelector);
				for (Element c : matches) {
					ret.add(c);
				}
			}
			else if (isMatching && s.innerSelector == null) {
				ret.add(e);
			}
			else {
				Element[] matches = getElementsMatchingSelector(e.children().toArray(new Element[] {}), s);
				for (Element c : matches) {
					ret.add(c);
				}
			}
		}
		return ret.toArray(new Element[] {});
 	}

	private static boolean live(SUrl s) {
		try {
			java.net.HttpURLConnection connection = (java.net.HttpURLConnection)new java.net.URL(s.url).openConnection();
			
			connection.setRequestMethod("HEAD");
			int responseCode = connection.getResponseCode();
			if (responseCode >= 200 && responseCode < 400) {
			    return true;
			}
			else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	private static SUrl[] live(SUrl[] u) {
		List<SUrl> ret = new ArrayList<SUrl>();
		for (SUrl s : u) {
			if (live(s)) {
				ret.add(s);
			}
		}
		return ret.toArray(new SUrl[] {});
	}


	public static int[] array(int... values){
		return values;
	}
	public static String[] array(String... values){
		return values;
	}
	public static boolean[] array(boolean... values){
		return values;
	}

}